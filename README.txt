#######################READMEN#############
PROJECT: Cucumber/Selenium Proof of Concept
Author : Kiem Tech
Versions : 1.0.0

###########################################
Framework : Cucumber, Selenium, Mavin, Java

##############
How to Install
    1. Download Maven, Crhome driver, Cucumber, Selenium

##########
How to Run
    1. In terminal, mvn test (in the root folder)
