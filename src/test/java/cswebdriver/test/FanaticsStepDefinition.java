package cswebdriver.test;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FanaticsStepDefinition {

    protected WebDriver driver;
    private String accountLinkResult;

    @Before
    public void setup() {
        driver = new FirefoxDriver();
    }

    @Given("^I open fanatics$")
    public void I_open_fanatics() {
        //Set implicit wait of 10 seconds and launch google
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://qc2-www.fanatics.corp/account/login");
    }

    @When("^I enter \"([^\"]*)\" in E-Mail Address textbox$")
    public void I_enter_in_email_textbox(String emailAddress) {
        //Write email in email textbox
        WebElement emailTextBox = driver.findElement(By.id("Login_UserName"));
        emailTextBox.sendKeys(emailAddress);
    }
    @When("^I enter \"([^\"]*)\" in Your Password textbox$")
    public void I_enter_in_your_password_textbox(String password) {
        //Write password in textbox
        WebElement passwordBox = driver.findElement(By.id("Login_Password"));
        passwordBox.sendKeys(password);

    }

    @When("^I click on Sign In Button$")
    public void I_click_on_Sign_In_Button() {
        //Click on sign in
        WebElement signInButton = driver.findElement(By.id("Login_Submit"));
        signInButton.click();
    }

    @Then("^Account page should equal to \"([^\"]*)\"$")
    public void accout_page_should_equal_to(String accountLink) throws Exception {
        accountLinkResult =  "https://qc2-www.fanatics.corp/account/home";
        Assert.assertEquals(accountLink, accountLinkResult);
        driver.close();
    }
    @After
    public void closeBrowser() {
        driver.quit();
    }

}


