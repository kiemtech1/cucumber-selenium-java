#This is the feature file which maps to the step def file

Feature: Log in to Fanatics website
    As an existing user, I want to load fanatics website
    So I can log in

   Scenario: Log in to Fanatics website
   Given I open fanatics
   When I enter "fanatics.tester@gmail.com" in E-Mail Address textbox
   When I enter "QAroxx123" in Your Password textbox
   When I click on Sign In Button
   Then Account page should equal to "https://qc2-www.fanatics.corp/account/home"
